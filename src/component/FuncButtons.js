import React from 'react'
export default class FuncButtons extends React.Component{

    constructor(props, context) {
        super(props, context);
    }
    render() {
        return <button onClick={this.props.callBack}>{this.props.context}</button>
    }
}
