import React from 'react';
import FuncButton from './component/FuncButtons'
import './App.css';

class App extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            firstNum: 0,
            secondNum: 0,
            expression: '',
            result: 0
        }
        this.funcButtonHandle = this.funcButtonHandle.bind(this);
        this.firstChange = this.firstChange.bind(this);
        this.secondChange = this.secondChange.bind(this);
    }

    funcButtonHandle(mode) {
        if (mode === 0) this.setState({expression: '+'});
        if (mode === 1) this.setState({expression: '-'});
        if (mode === 2) this.setState({expression: '*'});
        if (mode === 3) this.setState({expression: '/'});
        if (mode === 4) {
            this.setState({
                firstNum: 0,
                secondNum: 0,
                result: 0,
                expression: ''
            })
        }
        if (mode === 5) {
            if (this.state.expression === '+') this.setState({result: this.state.firstNum + this.state.secondNum});
            if (this.state.expression === '-') this.setState({result: this.state.firstNum - this.state.secondNum});
            if (this.state.expression === '*') this.setState({result: this.state.firstNum * this.state.secondNum});
            if (this.state.expression === '/') this.setState({result: this.state.firstNum / this.state.secondNum});
            if (this.state.expression === '%') this.setState({result: this.state.firstNum % this.state.secondNum});

        }
        if(mode===6)this.setState({expression: '%'})

    }

    firstChange(event) {
        this.setState({
            firstNum: event.target.value * 1
        })
    }

    secondChange(event) {
        this.setState({
            secondNum: event.target.value * 1
        })
    }

    render() {
        return (
            <div className="App">

                <br/>
                <input type={'text'} className={'result'} value={this.state.result}/>
                <input type={'text'} className={'firstNum'} onChange={this.firstChange} value={this.state.firstNum}/>
                <input type={'text'} className={'secondNum'} onChange={this.secondChange} value={this.state.secondNum}/>

                <FuncButton className={'add'} context={'+'} callBack={() => this.funcButtonHandle(0)}/>
                <FuncButton className={'subtract'} context={'-'} callBack={() => this.funcButtonHandle(1)}/>
                <FuncButton className={'multiply'} context={'*'} callBack={() => this.funcButtonHandle(2)}/>
                <FuncButton className={'divide'} context={'/'} callBack={() => this.funcButtonHandle(3)}/>
                <FuncButton className={'mod'} context={'%'} callBack={() => this.funcButtonHandle(6)}/>

                <hr/>

                <FuncButton className={'clear'} context={'AC'} callBack={() => this.funcButtonHandle(4)}/>
                <FuncButton className={'equal'} context={'='} callBack={() => this.funcButtonHandle(5)}/>

            </div>
        );
    }

}

export default App;
